# How to clear my browser cache.

## Dev Requirements

### node & npm

"Node.js® is a platform built on Chrome's JavaScript runtime".

Grunt (+ plugins) and Bower run on node.

[Install node globally](http://nodejs.org/download/).

### grunt

"The JavaScript Task Runner".

Node based task runner. Runs the build process.

### bower

"A package manager for the web".

Used to load frontend dependencies (e.g. third-party libraries like jQuery).

[Install bower globally](http://bower.io/#install-bower).

### ruby

"A dynamic, open source programming language with a focus on simplicity and productivity."

SASS is written in Ruby, so we need it.

Included on OS X. [Windows has an installer](http://rubyinstaller.org/).

### bundler

Package manager for ruby.

~~~~
# on *nix systems
sudo gem install bundler
~~~~

## Installing Dependencies

Once you have all the dev requirements installed, you can install the project dependencies:

~~~~
npm install

bundle install

bower install
~~~~

## Build

Then you can build the project.

~~~~
grunt
~~~~