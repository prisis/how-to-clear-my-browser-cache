//init blocks.Application
var
    App          = blocks.Application(),

    parser       = new UAParser(),

    browserName  = function getbrowserName() {
        var b = parser.getBrowser().name,
            v = parser.getBrowser().major,
            name;

        if (b === 'Chrome') {
            name = 'google chrome';
        } else if (b === 'IE' && v === 8) {
            name = 'internet explorer 8';
        } else if (b === 'IE' && v === 9) {
            name = 'internet explorer 9';
        } else if (b === 'IE' && v === 10) {
            name = 'internet explorer 10';
        } else if (b === 'IE' && v === 11) {
            name = 'internet explorer 11';
        } else if (b === 'Firefox') {
            name = 'mozilla firefox';
        } else if (b === 'Safari') {
            name = 'apple safari';
        } else if (b === 'Opera') {
            name = 'opera';
        } else if (b === 'Vivaldi') {
            name = 'vivaldi';
        }

        return name;
    },

    getOsBrowser,

    getBrowserName,

    getBrowserVersion,

    getOsName,

    ieVersion,

    addHtmlClasses,

    getLang = function lang() {
        var lang;

        if (navigator.languages) {
            // chrome does not currently set navigator.language correctly https://code.google.com/p/chromium/issues/detail?id=101138
            // but it does set the first element of navigator.languages correctly
            lang = navigator.languages[0];
        } else if (navigator.userLanguage) {
            // IE only
            lang = navigator.userLanguage;
        } else {
            // as of this writing the latest version of firefox + safari set this correctly
            lang = navigator.language;
        }

        return lang;
    },

    menu = function menu() {
        $('#langtrigger').click(function() {
            $('.lang-world-map').addClass('on').fadeToggle('slow');
        });
    };

//
getOsName         = parser.getOS().name.toLowerCase();
getBrowserName    = parser.getBrowser().name.toLowerCase();
getBrowserVersion = parser.getBrowser().version.toLowerCase();
ieVersion         = function ver() {
    if (getBrowserName === 'ie') {
        return '.' + getBrowserVersion;
    } else {
        return '';
    }
};
getOsBrowser      = getOsName + '.' + getBrowserName + ieVersion();

//
addHtmlClasses = function(lang) {
    $('html').addClass(lang);
};

// Add class to html element
$('html').addClass(getOsName + ' ' + getBrowserName + ' ' + getBrowserVersion + ' ' + getLang());

menu();
