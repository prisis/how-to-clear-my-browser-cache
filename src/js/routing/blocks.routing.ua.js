App.View('Ua', {
    // options object contains all properties that define the View behavior
    options: {
      // enabling the View routing and setting it to the root page
      route: '/ua'
    },

    browser: {
      icon: getBrowserName,
      name: browserName()
    },

    header : uaCollection['header'],

    list: uaCollection[getOsBrowser.replace(/\s+/g, '.')],

    routed: function () {
      if ($('.lang-world-map').hasClass('on')) {
        $('.lang-world-map').removeClass('on').fadeToggle('slow');
      }
    },

    // called when the view is created
    // do any initialization work here
    init: function () {
    }
});
