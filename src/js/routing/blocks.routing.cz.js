App.View('Cz', {
    // options object contains all properties that define the View behavior
    options: {
      // enabling the View routing and setting it to the root page
      route: '/cz'
    },

    browser: {
      icon: getBrowserName,
      name: browserName()
    },

    header : czCollection['header'],

    list: czCollection[getOsBrowser.replace(/\s+/g, '.')],

    routed: function () {
      if ($('.lang-world-map').hasClass('on')) {
        $('.lang-world-map').removeClass('on').fadeToggle('slow');
      }
    },

    // called when the view is created
    // do any initialization work here
    init: function () {
    }
});
