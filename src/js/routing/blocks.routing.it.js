App.View('It', {
    // options object contains all properties that define the View behavior
    options: {
      // enabling the View routing and setting it to the root page
      route: '/it'
    },

    browser: {
      icon: getBrowserName,
      name: browserName()
    },

    header : itCollection['header'],

    list: itCollection[getOsBrowser.replace(/\s+/g, '.')],

    routed: function () {
      if ($('.lang-world-map').hasClass('on')) {
        $('.lang-world-map').removeClass('on').fadeToggle('slow');
      }
    },

    // called when the view is created
    // do any initialization work here
    init: function () {
    }
});
