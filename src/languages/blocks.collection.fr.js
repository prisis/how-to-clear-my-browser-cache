var frCollection = {
    'header' : {
        title  : "Clearing Your Browser's Cache",
        text   : 'Usally a "force refresh" is enough. You can achieve this by pressing the following key combination:',
        windows: 'Bei Windows: <code>STRG + F5</code>',
        apple  : 'Bei Apple: <code>CMD + R</code>',
        subTitle: 'Detailed instructions to completely clear the cache.',
    },

    //Chrome
    'windows.chrome': Lists([{
        content: 'Click the menu <img src="assets/img/chrome-menu.png" alt="Chrome Menu"> icon in the top-right corner.'
    }, {
        content: 'Select <strong>History</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong>.'
    }, {
        content: 'Set <strong>Obliterate the following items from</strong> to <strong>the beginning of time.</strong>'
    }, {
        content: 'Check <strong>Cached images and files</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong> on the bottom. Your browser cache is now empty.'
    }, {
        content: 'Close and restart Chrome for the clearing to take full effect.'
    }]),
    'mac.os.chrome': Lists([{
        content: 'Click <strong>Chrome</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong>.'
    }, {
        content: 'Set <strong>Obliterate the following items from</strong> to <strong>the beginning of time.</strong>'
    }, {
        content: 'Check <strong>Cached images and files</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong> on the bottom. Your browser cache is now empty.'
    }, {
        content: 'Close and restart Chrome for the clearing to take full effect.'
    }]),
    'linux.chrome': Lists([{
        content: 'first article'
    }, {
        content: 'second article'
    }]),
    'ios.chrome': Lists([{
        content: 'From the Chrome app, tap the <strong>Menu</strong> icon'
    }, {
        content: 'Tap <strong>Settings</strong>.'
    }, {
        content: 'Tap <strong>Privacy</strong>.'
    }, {
        content: 'Tap <strong>Clear Cache</strong> and <strong>Clear Cookies, Site Data</strong>.'
    }]),
    'android.chrome': Lists([{
        content: 'From the Chrome app, tap the <strong>Menu</strong> icon'
    }, {
        content: 'Tap <strong>Settings</strong>.'
    }, {
        content: 'Tap <strong>Privacy</strong>.'
    }, {
        content: 'Tap <strong>Clear Browsing Data</strong>.'
    }, {
        content: 'Check the <strong>Clear the Cache</strong> and <strong>Clear Cookies, Site Data options</strong>.'
    }, {
        content: 'Tap <strong>Clear</strong>.'
    }]),

    //Firefox
    'windows.firefox': Lists([{
        content: 'Click the menu icon in the top-right corner.'
    }, {
        content: 'Select <strong>Select Options</strong>.'
    }, {
        content: 'Click the <strong>Advanced</strong> icon, then click <strong>Network</strong>.'
    }, {
        content: 'Under <strong>Cached Web Content</strong> click <strong>Clear Now</strong>.'
    }, {
        content: 'Close and restart Firefox for the clearing to take full effect.'
    }]),
    'mac.os.firefox': Lists([{
        content: 'Click the menu icon in the top-right corner.'
    }, {
        content: 'Select <strong>Preferences</strong>.'
    }, {
        content: 'Click the <strong>Advanced</strong> icon, then click <strong>Network</strong>.'
    }, {
        content: 'Under <strong>Cached Web Content</strong> click <strong>Clear Now</strong>.'
    }, {
        content: 'Close and restart Firefox for the clearing to take full effect.'
    }]),
    'linux.firefox': Lists([{
        content: 'first article'
    }, {
        content: 'second article'
    }]),

    //Safari
    'windows.safari': Lists([{
        content: 'Click <strong>Safari</strong>.'
    }, {
        content: 'Select <strong>Preferences</strong>.'
    }, {
        content: 'In the <strong>Advanced</strong> tab, check <strong>Show Develop menu in menu bar</strong>.'
    }, {
        content: 'Click <strong>Develop</strong>.'
    }, {
        content: 'Select <strong>Empty Caches</strong>.'
    }, {
        content: 'Close and restart Safari for the clearing to take full effect.'
    }]),
    'mac.os.safari': Lists([{
        content: 'Click <strong>Safari</strong>.'
    }, {
        content: 'Select <strong>Preferences</strong>.'
    }, {
        content: 'In the <strong>Advanced</strong> tab, check <strong>Show Develop menu in menu bar</strong>.'
    }, {
        content: 'Click <strong>Develop</strong>.'
    }, {
        content: 'Select <strong>Empty Caches</strong>.'
    }, {
        content: 'Close and restart Safari for the clearing to take full effect.'
    }]),
    'ios.safari': Lists([{
        content: 'From the <strong>Home</strong> screen, open the <strong>Settings App</strong>.'
    }, {
        content: 'Open <strong>Safari</strong> settings.'
    }, {
        content: 'Swipe to the bottom of the menu and tap <strong>Advanced</strong>.'
    }, {
        content: 'Tap <strong>Website Data</strong>.'
    }, {
        content: 'Swipe to the bottom of the menu and tap <strong>Remove all Website Data</strong>.'
    }]),

    //Internet Explorer
    'windows.ie.11.0': Lists([{
        content: 'Click the <img src="assets/img/gear-wheel.png" alt="gear cog"> icon in the upper-right corner.'
    }, {
        content: 'Select <strong>Safety</strong>, and then click <strong>Delete browsing history</strong>.'
    }, {
        content: 'Uncheck <strong>Preserve Favorites website data</strong>.'
    }, {
        content: 'Check <strong>Temporary Internet files and website files</strong> and <strong>Cookies and website data</strong>.'
    }, {
        content: 'Click <strong>Delete</strong>.'
    }, {
        content: 'Close and restart Internet Explorer for the clearing to take full effect.'
    }]),
    'windows.ie.10.0': Lists([{
        content: 'Click the <img src="assets/img/gear-wheel.png" alt="gear cog"> icon in the upper-right corner.'
    }, {
        content: 'Select <strong>Safety</strong>, and then click <strong>Delete browsing history</strong>.'
    }, {
        content: 'Uncheck <strong>Preserve Favorites website data</strong>.'
    }, {
        content: 'Check <strong>Temporary Internet files and website files</strong> and <strong>Cookies and website data</strong>.'
    }, {
        content: 'Click <strong>Delete</strong>.'
    }, {
        content: 'Close and restart Internet Explorer for the clearing to take full effect.'
    }]),
    //Todo
    'windows.ie.9.0': Lists([{
        content: 'Click the <img src="assets/img/gear-wheel.png" alt="gear cog"> icon in the upper-right corner.'
    }, {
        content: 'Select <strong>Safety</strong>, and then click <strong>Delete browsing history</strong>.'
    }, {
        content: 'Uncheck <strong>Preserve Favorites website data</strong>.'
    }, {
        content: 'Check <strong>Temporary Internet files and website files</strong> and <strong>Cookies and website data</strong>.'
    }, {
        content: 'Click <strong>Delete</strong>.'
    }, {
        content: 'Close and restart Internet Explorer for the clearing to take full effect.'
    }]),
    //Todo
    'windows.ie.8.0': Lists([{
        content: 'Click the <img src="assets/img/gear-wheel.png" alt="gear cog"> icon in the upper-right corner.'
    }, {
        content: 'Select <strong>Safety</strong>, and then click <strong>Delete browsing history</strong>.'
    }, {
        content: 'Uncheck <strong>Preserve Favorites website data</strong>.'
    }, {
        content: 'Check <strong>Temporary Internet files and website files</strong> and <strong>Cookies and website data</strong>.'
    }, {
        content: 'Click <strong>Delete</strong>.'
    }, {
        content: 'Close and restart Internet Explorer for the clearing to take full effect.'
    }]),

    //Vivaldi
    'windows.vivaldi': Lists([{
        content: 'Open a new <strong>Speed Dial (Tab)</strong>.'
    }, {
        content: 'Click on <strong>History</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong>.'
    }, {
        content: 'Set <strong>Obliterate the following items from</strong> to <strong>the beginning of time.</strong>'
    }, {
        content: 'Check <strong>Cached images and files</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong> on the bottom. Your browser cache is now empty.'
    }, {
        content: 'Close and restart Vivaldi for the clearing to take full effect.'
    }]),
    'mac.os.vivaldi': Lists([{
        content: 'Open a new <strong>Speed Dial (Tab)</strong>.'
    }, {
        content: 'Click on <strong>History</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong>.'
    }, {
        content: 'Set <strong>Obliterate the following items from</strong> to <strong>the beginning of time.</strong>'
    }, {
        content: 'Check <strong>Cached images and files</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong> on the bottom. Your browser cache is now empty.'
    }, {
        content: 'Close and restart Vivaldi for the clearing to take full effect.'
    }]),
    'linux.vivaldi': Lists([{
        content: 'Open a new <strong>Speed Dial (Tab)</strong>.'
    }, {
        content: 'Click on <strong>History</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong>.'
    }, {
        content: 'Set <strong>Obliterate the following items from</strong> to <strong>the beginning of time.</strong>'
    }, {
        content: 'Check <strong>Cached images and files</strong>.'
    }, {
        content: 'Click <strong>Clear browsing data</strong> on the bottom. Your browser cache is now empty.'
    }, {
        content: 'Close and restart Vivaldi for the clearing to take full effect.'
    }]),

    //Opera
    'windows.opera': Lists([{
        content: 'Key combination: <code>ALT + p</code> or click on <strong>Opera</strong> in the top left corner.'
    }, {
        content: 'Click on <strong>Settings</strong>.'
    }, {
        content: 'Click on <strong>Privacy & security</strong> in the menu.'
    }, {
        content: 'Click on <strong>Clear browsing data...</strong>'
    }, {
        content: 'Click on <strong>Clear browsing data</strong> in the pop-up box.'
    }, {
        content: 'Close and restart Opera for the clearing to take full effect.'
    }]),
    'mac.os.opera': Lists([{
        content: 'Click on <strong>Opera</strong> in the top left corner.'
    }, {
        content: 'Click on <strong>Settings</strong>.'
    }, {
        content: 'Click on <strong>Privacy & security</strong> in the menu.'
    }, {
        content: 'Click on <strong>Clear browsing data...</strong>'
    }, {
        content: 'Click on <strong>Clear browsing data</strong> in the pop-up box.'
    }, {
        content: 'Close and restart Opera for the clearing to take full effect.'
    }]),
    'linux.opera': Lists([{
        content: 'Open a new <strong>Speed Dial (Tab)</strong>.'
    }, {
        content: 'Click on <strong>History</strong>.'
    }]),
};