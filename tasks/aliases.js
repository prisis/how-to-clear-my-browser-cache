module.exports = {
    'dev:test' : [
        'newer:jsonlint:dev',
        //'newer:jshint:dev',
        'newer:jsvalidate:dev'
    ],
    'default': [
        'newer:jsonlint',
        //'jshint',
        'clean',
        'newer:copy',
        'newer:uglify',
        'newer:sass',
        'newer:jade'
    ],
    'build': [
        'newer:lint'
    ]
};