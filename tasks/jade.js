'use strict';
//jade config

module.exports = {
    dist: {
        options: {
            pretty: true
        },
        files: [{
            cwd: 'src/jade',
            expand: true,
            src: '*.jade',
            dest: 'dist',
            ext: '.html'
        }]
    }
};
