'use strict';
//sass config

module.exports = {
    dist: {
        options: {
            style: 'compressed',
            loadPath: [
                'bower_components/bootstrap-sass/assets/stylesheets'
            ],
            bundleExec: true
        },
        files: {
            'dist/assets/css/combined.min.css': 'src/scss/main.scss'
        }
    }
};
