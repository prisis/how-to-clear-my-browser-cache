'use strict';
//uglify config

module.exports = {
    dist: {
        options: {
            sourceMap: true,
            drop_console: true
        },
        files: {
            'dist/assets/js/combined.min.js': [
                'bower_components/jquery/dist/jquery.js',
                'bower_components/blocks/dist/blocks.js',
                'bower_components/ua-parser-js/src/ua-parser.js',

                'src/js/blocks.js',

                'src/js/model/blocks.model.list.js',

                'src/languages/blocks.collection.br.js',
                'src/languages/blocks.collection.cn.js',
                'src/languages/blocks.collection.cz.js',
                'src/languages/blocks.collection.de.js',
                'src/languages/blocks.collection.ee.js',
                'src/languages/blocks.collection.en.js',
                'src/languages/blocks.collection.es.js',
                'src/languages/blocks.collection.fr.js',
                'src/languages/blocks.collection.hu.js',
                'src/languages/blocks.collection.it.js',
                'src/languages/blocks.collection.nl.js',
                'src/languages/blocks.collection.pl.js',
                'src/languages/blocks.collection.pt.js',
                'src/languages/blocks.collection.ru.js',
                'src/languages/blocks.collection.si.js',
                'src/languages/blocks.collection.ua.js',

                'src/js/routing/blocks.routing.br.js',
                'src/js/routing/blocks.routing.cn.js',
                'src/js/routing/blocks.routing.cz.js',
                'src/js/routing/blocks.routing.de.js',
                'src/js/routing/blocks.routing.ee.js',
                'src/js/routing/blocks.routing.en.js',
                'src/js/routing/blocks.routing.es.js',
                'src/js/routing/blocks.routing.fr.js',
                'src/js/routing/blocks.routing.home.js',
                'src/js/routing/blocks.routing.hu.js',
                'src/js/routing/blocks.routing.it.js',
                'src/js/routing/blocks.routing.nl.js',
                'src/js/routing/blocks.routing.pl.js',
                'src/js/routing/blocks.routing.pt.js',
                'src/js/routing/blocks.routing.ru.js',
                'src/js/routing/blocks.routing.si.js',
                'src/js/routing/blocks.routing.ua.js'
            ]
        }
    },
    ie: {
        options: {
            sourceMap: true,
            drop_console: true
        },
        files: {
            'dist/assets/js/combined-ie.min.js': [
                'src/js/main-ie.js'
            ]
        }
    }
};