'use strict';
//clean config

module.exports = {
    all: {
        src: 'src/img/browser/*.png',

        destImg: 'src/img/browser-spritesheet.png',
        destCSS: 'src/scss/_browser-sprites.scss',

        algorithm: 'binary-tree',

        imgPath: 'img/browser-spritesheet.png',

        'cssVarMap': function (sprite) {
            sprite.name = 'logosprite-' + sprite.name;
        }
    }
};