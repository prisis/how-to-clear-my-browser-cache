'use strict';
//copy config

module.exports = {
    html5shiv: {
        files: [
            {
                expand: true,
                cwd: 'bower_components/html5shiv/dist',
                src: 'html5shiv.js',
                dest: 'dist/assets/js'
            }
        ]
    },
    img: {
        files: [
            {
                expand: true,
                cwd: 'src/img',
                src: '**.*',
                dest: 'dist/assets/img'
            }
        ]
    },
};